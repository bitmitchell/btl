#pragma once
#include <cstdint>
#include <vector>

#if !defined(BTL_SLOT_MAP_DEFAULT_CHUNK_SIZE)
#	define BTL_SLOT_MAP_DEFAULT_CHUNK_SIZE		32
#endif

namespace btl {

	// The slot_map class behaves like an unordered_map<handle, T>. The only difference is it handles it's own memory and is much more efficient.
	template <class T, int chunk_size = BTL_SLOT_MAP_DEFAULT_CHUNK_SIZE>
	class slot_map {
	public:
		typedef uint64_t handle;

	private:
		// id_wrapper struct simply adds an id field onto the T obj when it is allocated. This is used to double check valid objects are returned.
		struct id_type {
			uint64_t id;
			bool alive;
			T obj;
		};

		std::vector<id_type*> chunk_table;
		std::vector<uint32_t> free_list;

		id_type* get_id(handle id) {
			return chunk_table[(id & 0xFFFFFFFF) / chunk_size] + ((id & 0xFFFFFFFF) % chunk_size);
		}

	public:

		class iterator {
			slot_map* map;
			uint32_t index;

			id_type* get() {
				return &map->chunk_table[index / chunk_size][index % chunk_size];
			}

			void increment() {
				do {
					index++;
				} while (index < map->chunk_table.size() * chunk_size && !get()->alive);
			}
		public:
			typedef iterator self_type;
			typedef T value_type;
			typedef T& reference;
			typedef T* pointer;
			typedef std::forward_iterator_tag catergory;
			typedef int difference_type;
			iterator(slot_map* map) : map(map), index(0) {}
			self_type operator++() { self_type i = *this; increment(); return i; }
			self_type operator++(int) { increment(); return *this; }
			reference operator*() { return get()->obj; }
			pointer operator->() { return &get()->obj; }
			bool operator==(const self_type& rhs) {
				return map == rhs.map && index == rhs.index && index < map->chunk_table.size() * chunk_size;
			}
			bool operator!=(const self_type& rhs) {
				return index < map->chunk_table.size() * chunk_size && (map != rhs.map || index != rhs.index);
			}
		};


		template <class ...Args>
		handle create(Args&&... args) {
			if (free_list.empty()) {
				id_type* chunk = new id_type[chunk_size];
				for (int i = chunk_size - 1; i >= 0; --i) {
					chunk[i].id = (uint32_t)chunk_table.size() * chunk_size + i;
					chunk[i].alive = false;
					free_list.push_back((uint32_t)chunk_table.size() * chunk_size + i);
				}
				chunk_table.push_back(chunk);
			}

			uint32_t free = free_list.back();
			free_list.pop_back();

			id_type& obj = chunk_table[free / chunk_size][free % chunk_size];
			obj.alive = true;
			// inplace construct the new T obj and forward the arguments to it.
			new (&obj.obj) T(std::forward<Args>(args)...);
			return obj.id;
		}

		T* get(handle id) {
			id_type* obj = get_id(id);
			return obj->id == id ? &obj->obj : nullptr;
		}

		void destory(handle id) {
			id_type* obj = get_id(id);
			obj->obj.~T();
			obj->alive = false;
			obj->id = (obj->id & 0xFFFFFFFF) | (obj->id + ((uint64_t)1 << 32));
			free_list.push_back(id & 0xFFFFFFFF);
		}

		iterator begin() {
			return iterator(this);
		}

		iterator end() {
			return iterator(nullptr);
		}
	};

}
