#pragma once
#include <algorithm>
#include <vector>
#include <functional>

namespace btl {

	template <class ...Args>
	class event;

	template <class ...Args>
	class listener {
		event<Args...>* parent;
		friend class event<Args...>;
	public:
		virtual ~listener() {
			parent->remove(this);
		}

		virtual void on_event(Args...) = 0;
	};


	template <class ...Args>
	class event {
		std::vector<listener<Args...>*> clients;
	public:
		void attach(listener<Args...>* client) {
			client->parent = this;
			clients.push_back(client);
		}

		void remove(listener<Args...>* client) {
			clients.erase(std::remove(clients.begin(), clients.end(), client), clients.end());
		}

		void call(Args... args) {
			for (auto client : clients) {
				client->on_event(args...);
			}
		}

		void operator()(Args... args) {
			call(args...);
		}
	};


	template <class ...Args>
	class delegate_listener : public listener<Args...> {
	protected:
		void on_event(Args... args) {
			event(args...);
		}
	public:
		std::function<void(Args...)> event;
	};

}