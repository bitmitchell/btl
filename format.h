#pragma once
#include <cstring>
#include <string>
#include <sstream>
#include <cassert>


#if !defined(FORMAT_DELIMITER)
#	define FORMAT_DELIMITER		"%%%"
#endif


namespace btl {

	inline std::string format(const std::string& str) {
		assert(str.find(FORMAT_DELIMITER) == str.npos && "Too few arguments for format string");
		return str;
	}


	template <class T, class ...U>
	std::string format(const std::string& str, const T& val, const U&... args) {
		auto i = str.find(FORMAT_DELIMITER);
		assert(i != str.npos && "Too many arugments for format string");

		std::stringstream stream;
		stream << str.substr(0, i) << val << format(str.substr(i + std::strlen(FORMAT_DELIMITER)), args...);
		return stream.str();
	}

}
