# BiT's Template Library

A small header only collection of template classes that might come in handy for game development. These all support full C++11 STL integration.

Currently this library includes the following:
* slot_map
